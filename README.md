# Søk for Trondheim folkebibliotek
Dette er et klientsidesøk for Trondheim folkebibliotek. Søket bruker API-et [_Search and retrieve_](http://bibliofil.no/kundeinfo/dokumentasjon/web/webapi/webapi-uthenting.html) fra [Bibliofil](http://bibliofil.no).

## Strategi for søk
[search-service.ts](src/search-service.ts) inneholder strategien til søket. Stategien er å søke i en gitt rekkefølge:

1. Forfatter ordrett.
2. Forfatter inneholder alle ord.
3. Tittel ordrett.
4. Tittel inneholder alle ord.
5. Hvor som helst ordrett.
6. Hvor som helst inneholder alle ord.

Hvis ønsket antall treff er funnet, `WANTED_RECORDS`, vil søket stoppe og sette `has_more_results` til `true`.

Dersom en ønsker flere treff kan `get_more_results` brukes, som vil fortsette søket der det slapp. Enten:

- Samme strategi har flere treff som ikke er hentet.
- Neste strategi.

## Tilstand
Tilstand er delt opp i:

- `InputState`: Rå tilstand til input-felt som trengs for API-kallene.
- `NormalizedInputState`: Normalisert `InputState`. Endring vil resette og kjøre søket på nytt. Skal kun inneholde tilstand som vil kreve et nytt API-kall. Normalisering er slik som å fjerne spesial-karakterer, ekstra mellomrom, osv.
- `SearchState`: Søkeresultatet som skal vises til brukeren.
- `InternalSearchState`: Intern tilstand som holder oversikt over hva som er neste strategi, osv.
- `FilterState`: For bruk til klient-side filtrering. Populeres av søkeresultatet, eksempelvis hvilke forfattere som resultatet består av.

## Sortering
Sortering gjøres med en poengsum som kalkuleres i [score.ts](src/score.ts). Treff med høyere poengsum vil komme først i listen, prioritert etter:

1. Forfatter:  antall treff med søketekst.
2. Tittel: antall treff med søketekst.
3. Dato: nyere > eldre.
4. Språk: nob > mul.
5. Format: bok > lyd > blu-ray > dvd.

## Utvikling

### Publisere
```sh
# endre versjon
npm version major|minor|patch
# pakk til sok-trondheim-folkebibliotek-x.y.z.zip
npm run build
```

Pakk ut filene fra zip-filen under `/tfbs/` på nettstedet og link til [internal-bootstrap.js](public/internal-bootstrap.js)
i template / HTML:

```html
<script src="/tfbs/internal-bootstrap.js"></script>
```

### Tester
Se [test/README.md](test/README.md) for tester.

### Stil
Stilene ligger i CSS-filer som importeres i komponenten de brukes i, eventuelt i rot-stilen `App.css`. Klasser og id-er er prefikset med `tfbs-`.

### Publisering
Scriptet [push-to-pages.sh](push-to-pages.sh) kan brukes i Bitbucket Pipelines for å bygge automatisk og publisere til Bitbucket Pages.