import { SearchResult } from "./sru-request-parsing";

export function wait(timeout: number) {
    return new Promise(resolve => {
        setTimeout(resolve, timeout);
    });
}

export function get_search_text_from_url(): string {
    let hash = window.location.hash.slice(1)

    if (hash.length === 0 || hash.match(/^tfbs:/) === null) {
        return ""
    }

    let [, search_text] = hash.match(/^tfbs:(.*)/);
    return decodeURI(search_text);
}

let previous_set_search_text: NodeJS.Timeout;
export function set_search_text_in_url_debounced(text: string): void {
    clearTimeout(previous_set_search_text);

    previous_set_search_text = setTimeout(() => {
        if (text.trim().length) {
            window.location.hash = "tfbs:" + encodeURI(text)
        } else {
            window.location.hash = "tfbs"
        }
    }, 500);
}

/**
 * Setter cookie som leses av Mine lån og reservasjoner på
 * https://biblioteket.trondheim.kommune.no/mine-sider/mine-lan-og-reservasjoner/
 */
export function set_reserver_cookie(entry: SearchResult) {
    // nextAction=reserver:localid
    let one_minute_from_now = new Date(Date.now() + 60 * 1000).toUTCString();
    let cookie = `nextAction=reserver:${entry.localid}; expires=${one_minute_from_now}; path=/`;
    document.cookie = cookie;
}

/**
 * Helper for updating state values, keeping event.target.value before calling setter.
 */
export function updateValue<T>(event: React.ChangeEvent<HTMLInputElement>, key: keyof T, setter: Setter<T>) {
    // target will disappear on syntetic event,
    // keep value before passing it to setter callback
    let value = event.target.value;

    setter(prevValue => ({
        ...prevValue,
        [key]: value,
    }))
}

interface Setter<T> {
    (fn: (prevState: T) => T): void
}

export function updateCheckedValue<T>(event: React.ChangeEvent<HTMLInputElement>, key: keyof T, setter: Setter<T>) {
    // target will disappear on syntetic event,
    // keep value before passing it to setter callback
    let value = event.target.checked;

    setter(prevValue => ({
        ...prevValue,
        [key]: value,
    }))
}

export function unique<T>(values: T[]): T[] {
    return Array.from((new Set(values)).values())
}