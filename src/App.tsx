import React, { useState } from "react";
import "./App.css";
import useSearchState from "./search-state";
import { useInputState } from "./input/input-state";
import { FilterInput } from "./filter/FilterInput";
import { SearchInput } from "./input/SearchInput";
import { SearchOutput } from "./output/SearchOutput";
import { useFilterState } from "./filter/filter-state";

const App: React.FC = () => {
    // input will dispatch new search
    let [inputState, normalizedInputState, setInputState] = useInputState();
    let [searchState, get_more_results] = useSearchState(normalizedInputState);
    // client side filters
    let [filterState, setFilterState] = useFilterState(searchState);
    let [searchEnabled, setSearchEnabled] = useState(inputState.searchText !== "");

    let searchEnabledClass = searchEnabled ? "enabled" : "";
    return (
        <div>
            <div id="tfbs-overlay" className={searchEnabledClass} />
            <div id="tfbs-app" className={searchEnabledClass}>
                {searchEnabled &&
                    <FilterInput inputState={inputState} setInputState={setInputState} filterState={filterState} setFilterState={setFilterState} searchState={searchState} />
                }
                <SearchInput inputState={inputState} setInputState={setInputState} searchEnabled={searchEnabled} setSearchEnabled={setSearchEnabled} />
                {searchEnabled && normalizedInputState.searchText.length > 2 &&
                    <SearchOutput input={normalizedInputState} filters={filterState} searchState={searchState} getMoreResults={() => get_more_results()} />
                }
            </div>
        </div>);
};

export default App;
