import { language_enum, Language } from "./Language";

export interface APIResult {
    next_record_position: string,
    records: SearchResult[],
}

export interface SearchResult {
    title: string,
    creator: string,
    img: string,
    description: string,
    publisher: string,
    date: string,
    format: string,
    genre: string,
    omfangfull: string,
    isbn: string,
    localid: string,
    bibliofilid: string,
    hylle: string,
    hyllesignatur: string,
    language: Language,
    score: number,
    original_title: string | undefined,
}

export function xml_to_json(xml: string): APIResult {
    const doc = new DOMParser().parseFromString(xml, "text/xml");
    let records = [];

    const next_record_position = get_inner(doc, "nextRecordPosition");

    for (let record of doc.querySelectorAll("record")) {
        let title = get_inner(record, "title");
        let creator = firstname_lastname(get_inner(record, "creator"));
        let img = get_inner(record, "krydderbildeurl");
        let description = get_inner(record, "description");
        let publisher = get_inner(record, "publisher");
        let date = get_inner(record, "date");
        let format = get_inner(record, "format");
        let genre = get_inner(record, "genre");
        let omfangfull = get_inner(record, "omfangfull");

        if (format === "lydopptak") {
            format = "Lydbok";
        }

        let isbn = get_inner(record, "#isbn");
        let localid = get_inner(record, "#localid");
        let bibliofilid = get_inner(record, "#bibliofilid");
        let hylle = get_inner(record, "hylle");
        let hyllesignatur = get_inner(record, "hyllesignatur");
        let language = language_enum(get_inner(record, "language"));

        let score = 0;

        records.push({
            title,
            creator,
            img,
            description,
            publisher,
            date,
            format,
            genre,
            omfangfull,
            isbn,
            localid,
            bibliofilid,
            hylle,
            hyllesignatur,
            language,
            score,
            original_title: undefined,
        });
    }

    return {
        next_record_position,
        records,
    };
}

export function get_inner(element: Document | Element, selector: string): string {
    let inner = element.querySelector(selector);
    if (!inner) {
        return "";
    }
    return inner.innerHTML;
}

export function firstname_lastname(name: string): string {
    if (name.includes(",")) {
        let [last, ...rest] = name.split(",");
        return rest.map(n => n.trim()).join(" ") + " " + last.trim();
    } else {
        return name;
    }
}

