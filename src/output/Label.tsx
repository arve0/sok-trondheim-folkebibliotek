import React from "react"
import "./Label.css"

export default function Label(props: { text: String }) {
    return <span className="tfbs-label">{props.text}</span>
}