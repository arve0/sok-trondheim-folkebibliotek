import React from "react";
import { SearchResult } from "../sru-request-parsing";
import "./SearchEntry.css";
import bildeMangler from "./bilde-mangler.png";
import Label from "./Label";
import { set_reserver_cookie } from "../utils";
import { Language, language_to_string } from "../Language";

export function SearchEntry(entry: SearchResult) {
    return (
        <div
            key={entry.localid}
            className="tfbs-entry"
            data-localid={entry.localid}
        >
            <div className="tfbs-entry-header">
                <h2 className="tfbs-entry-title">
                    {entry.title} ({entry.format}, {entry.date})
                </h2>
            </div>

            <div className="tfbs-entry-body">
                <div className="tfbs-entry-image">
                    {entry.img && <img src={entry.img} alt="forside" />}
                    {!entry.img && (
                        <img src={bildeMangler} alt="forsidebilde mangler" />
                    )}
                </div>
                <div className="tfbs-entry-details-container">
                    <div className="tfbs-entry-details">
                        {entry.creator && <p className="tfbs-creator">Forfatter: {entry.creator}</p>}
                        {entry.description && <p>{entry.description}</p>}
                        {entry.hylle && <Label text={"Hylleplass " +entry.hylle} />}
                        {entry.genre && <Label text={entry.genre} />}
                        {entry.language !== Language.Ukjent && <Label text={language_to_string(entry.language)} />}
                        {entry.omfangfull && <Label text={entry.omfangfull} />}
                        {entry.original_title &&
                            <p>Oversettelse av {entry.original_title}.</p>
                        }
                        {/* <p>
                            isbn: {entry.isbn} - lokalid: {entry.localid} - hyllesignatur: {entry.hyllesignatur}
                        </p> */}
                    </div>
                    <div className="tfbs-entry-buttons">
                        <Reserver entry={entry} />
                        <VisMer entry={entry} />
                    </div>
                </div>
            </div>
        </div>
    );
}

function VisMer(props: { entry: SearchResult }) {
    const base = "https://trondheim.bib.no/cgi-bin/m2?mode=p&tnr=";
    const url = base + props.entry.localid;

    return <a href={url}><button className="white">Vis mer</button></a>;
}

function Reserver(props: { entry: SearchResult}) {
    let host = window.location.host;
    let href = `https://${host}/mine-sider/mine-lan-og-reservasjoner/`;
    return (
        <a
            href={href}
            onClick={() => set_reserver_cookie(props.entry)}
        >
           <button>Reserver</button>
        </a>
    )
}
