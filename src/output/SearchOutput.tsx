import React from "react";
import { SearchEntry } from "./SearchEntry";
import { NormalizedInputState } from "../normalized-input-state";
import { SearchState } from "../search-state";
import "./SearchOutput.css";
import { FilterState } from "../filter/filter-state";
import { language_to_string } from "../Language";

export const SearchOutput: React.FC<SearchOutputProps> = ({
    input,
    filters,
    searchState,
    getMoreResults,
}) => {
    let visible_search_results = searchState.search_results;

    if (filters.selected.languages.length > 0) {
        visible_search_results = visible_search_results.filter(r => filters.selected.languages.includes(language_to_string(r.language)));
    }

    if (filters.selected.creators.length > 0) {
        visible_search_results = visible_search_results.filter(r => filters.selected.creators.includes(r.creator));
    }

    if (filters.selected.formats.length > 0) {
        visible_search_results = visible_search_results.filter(r => filters.selected.formats.includes(r.format));
    }

    // translation filter is ignored when other filters are enabled
    // avoids showing empty results when creator selected and book is translated
    if ([filters.selected.languages, filters.selected.creators, filters.selected.formats].every(v => v.length === 0)) {
        visible_search_results = visible_search_results.filter(r =>
            filters.includeTranslations ? true : r.original_title === undefined,
        );
    }

    let no_results =
        input.searchText !== "" &&
        !searchState.is_loading &&
        !searchState.has_more_results &&
        visible_search_results.length === 0;

    return (
        <div id="tfbs-results">
            {visible_search_results.map(SearchEntry)}
            {no_results && <p>Fant ingen treff på '{input.searchText}'.</p>}

            {searchState.is_loading && (
                <p>Søker etter '{input.searchText}' ...</p>
            )}

            {searchState.has_more_results && (
                <button onClick={getMoreResults}>Hent flere</button>
            )}

            {searchState.error && (
                <p className="tfbs-error">
                    En feil oppstod: {searchState.error}
                </p>
            )}
        </div>
    );
};

interface SearchOutputProps {
    input: NormalizedInputState,
    filters: FilterState,
    searchState: SearchState,
    getMoreResults: () => void,
}
