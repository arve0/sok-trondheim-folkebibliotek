import { useState, useEffect } from "react";
import axios, { CancelTokenSource, CancelToken, AxiosResponse, AxiosError } from "axios";
import calculate_score from "./score";
import { get_strategies, Strategy } from "./search-strategies";
import { xml_to_json, SearchResult } from "./sru-request-parsing";
import { NormalizedInputState } from "./normalized-input-state";
import { get_search_text_from_url } from "./utils";
import { defaultFilterOptions, createFilterOptions, FilterOptionsOrSelections } from "./filter/filter-options";

/**
 * CONSTANTS
 */
const WANTED_RECORDS = 10;
// retrieve two more then wanted, as result usually contain duplicates
// -> avoids two calls when strategy has plentyfull of results
const GET_N_RECORDS = WANTED_RECORDS + 2;
const BASE = "https://trondheim.bib.no/cgi-bin/";
const BASE_SRU = BASE +
    "sru" +
    "?operation=searchRetrieve" +
    `&maximumRecords=${GET_N_RECORDS}` +
    "&sortKeys=date,,false" +
    "&recordSchema=bsdc" +
    "&query=";
const BASE_REST = BASE + "rest_service/items/1.0/data/"
const BASE_CORS = "https://cors.seljebu.no/";
// API have CORS enabled for trondheim.kommune.no and localhost
const CORS_IF_BITBUCKET = window.location.host === "arve0.bitbucket.io"
    ? BASE_CORS
    : "";

const defaultSearchState: SearchState = {
    is_loading: get_search_text_from_url() !== "",
    search_results: [],
    has_more_results: false,
    error: null,
    filter_options: defaultFilterOptions,
}

/**
 * Søk etter bøker i følgende rekkefølge:
 *
 * - dc.creator="verbatim søketekst"
 * - dc.creator=verbatim and dc.creator=søketekst
 * - dc.title="verbatim søketekst"
 * - dc.title=verbatim and dc.title=søketekst
 * - cql.anywhere="verbatim søketekst"
 * - cql.anywhere=verbatim and cql.anywhere=søketekst
 *
 * Endepunkt: https://trondheim.bib.no/cgi-bin/sru
 * Query:
 *  operation=searchRetrieve
 *  recordSchema=bsdc
 *  query=søket (dc.title, dc.creator, cql.anywhere, dc.date, ...)
 */
export default function useSearchState(input: NormalizedInputState): [SearchState, () => void] {
    let [searchState, setSearchState] = useState<SearchState>(defaultSearchState);

    // when state changes, this function is called
    useEffect(() => {
        setSearchState(defaultSearchState)
        if (input.searchText.length < 3) {
            setSearchState({ ...defaultSearchState, is_loading: false })
            return;
        }

        let internalSearchState = create_internal_state();
        let timeout = setTimeout(() => perform_search(input, setSearchState, internalSearchState), 100);

        return () => {
            clearTimeout(timeout);
            internalSearchState.cancel_source.cancel();

            // mutate object, such that perform_search can avoid
            // updating state when we should abort
            internalSearchState.is_cancelled = true;
        };
    }, [input]);

    function get_more_results() {
        perform_search(input, setSearchState)
            .catch(errorHandler(setSearchState))
    }

    return [searchState, get_more_results];
}

function create_internal_state(): InternalSearchState {
    return {
        cancel_source: axios.CancelToken.source(),
        strategies: get_strategies(),
        next_record_position: null,
        is_cancelled: false,
    }
}

let internalSearchState: InternalSearchState | null = null;
async function perform_search(input: NormalizedInputState, setSearchState: SetSearchState, newState?: InternalSearchState) {
    let state: InternalSearchState
    if (newState) {
        internalSearchState = newState;
        state = newState;
    } else if (internalSearchState !== null) {
        state = internalSearchState;
    } else {
        throw new Error("perform_search called without internal state.")
    }

    setSearchState(prevState => ({
        ...prevState,
        is_loading: true,
    }))

    let got_n_more_results = 0;
    function increment_got_n_more_results(inc: number) {
        got_n_more_results += inc;
    }

    while (!state.is_cancelled && state.strategies.length > 0 && got_n_more_results < WANTED_RECORDS) {
        let strategy = state.strategies.shift();
        if (!strategy) {
            throw new Error("Unreachable. Unable to get strategy.");
        }

        let query = strategy(input.searchText);
        if (!query) {
            state.next_record_position = null;
            continue;
        }
        query += getAPIFiltersQuery(input);
        let response = await get(
            BASE_SRU,
            query,
            state.next_record_position,
            state.cancel_source.token,
        );
        let result = xml_to_json(response.data);
        await get_original_titles(result.records, state.cancel_source.token);
        result.records = remove_klassesett(result.records);
        result.records = remove_duplicates_within_same_result(result.records);
        result.records = filter_on_numbers(result.records, input.searchText)
        result.records = result.records.map(r => ({
            ...r,
            score: calculate_score(r, input.searchText),
        }));
        result.records.sort((a, b) => b.score - a.score);

        if (result.next_record_position) {
            state.strategies = [strategy, ...state.strategies];
            state.next_record_position = result.next_record_position;
        } else {
            state.next_record_position = null;
        }

        if (state.is_cancelled) {
            return;
        }

        setSearchState(prevState => {
            result.records = remove_duplicates(
                result.records,
                prevState.search_results,
            );

            // avoid reference to got_n_more_results
            increment_got_n_more_results(result.records.length);

            return {
                ...prevState,
                search_results: prevState.search_results.concat(result.records),
            }
        });

    }

    if (state.is_cancelled) {
        return;
    }

    setSearchState(prevState => ({
        ...prevState,
        is_loading: false,
        has_more_results: state.strategies.length > 0,
        filter_options: createFilterOptions(prevState)
    }))
}

function get(
    base: string,
    query: string,
    start_record: string | null,
    cancelToken: CancelToken,
): Promise<AxiosResponse> {
    if (start_record !== null) {
        query += `&startRecord=${start_record}`;
    }
    return axios.get(CORS_IF_BITBUCKET + base + query, { cancelToken });
}

function remove_klassesett(results: SearchResult[]): SearchResult[] {
    return results.filter(r => r.hyllesignatur.toLowerCase().match(/^(kl|lesesett)/) === null)
}

function remove_duplicates_within_same_result(results: SearchResult[]): SearchResult[] {
    let without_duplicates = [];
    for (let result of results) {
        if (is_not_in_set(result, without_duplicates)) {
            without_duplicates.push(result);
        }
    }
    return without_duplicates;
}

function remove_duplicates(
    new_results: SearchResult[],
    existing_results: SearchResult[],
): SearchResult[] {
    return new_results.filter(r => is_not_in_set(r, existing_results));
}

function is_not_in_set(entry: SearchResult, entries: SearchResult[]): boolean {
    return entries.every(existing_entry => {
        let id_different = entry.localid !== existing_entry.localid;
        let isbn_different =
            entry.isbn !== "" ? entry.isbn !== existing_entry.isbn : true;
        return id_different && isbn_different;
    });
}

async function get_original_titles(records: SearchResult[], cancel_token: CancelToken) {
    if (records.length === 0) {
        return;
    }
    let localids = records.map(r => r.localid).join(",");

    let items = await get(BASE_REST, localids, null, cancel_token);

    set_original_title(records, items);

    return records;
}

function set_original_title(records: SearchResult[], items: AxiosResponse<any>) {
    if (typeof items.data !== "object") {
        throw new Error("Unexpeted response from " + BASE_REST)
    }

    for (let key in items.data) {
        if (!items.data.hasOwnProperty(key) || isNaN(parseInt(key))) {
            continue;
        }

        let item = items.data[key];
        let record = records.find(r => r.localid === key);

        if (!item) {
            console.error("No item for key " + key)
            continue
        } else if (!record) {
            console.error("No record for key " + key)
            continue
        }

        if (!Array.isArray(item.Opplysninger)) {
            continue;
        }
        let prefix = "Originaltittel: ";
        let original_title = item.Opplysninger.find((o: any) => typeof o === "string" && o.indexOf(prefix) === 0)
        if (original_title) {
            record.original_title = original_title.slice(prefix.length)
        }
    }
}

function filter_on_numbers(records: SearchResult[], text: string): SearchResult[] {
    let numbers = text.match(/[0-9]+/g);
    if (numbers !== null) {
        let ns: string[] = numbers;
        return records.filter(r => record_contains_number(r, ns));
    } else {
        return records;
    }
}

function record_contains_number(record: SearchResult, numbers: string[]): boolean {
    return numbers.some(number => record.date.includes(number) || record.title.includes(number));
}

export interface SearchState {
    is_loading: boolean,
    search_results: SearchResult[],
    has_more_results: boolean,
    error: string | null,
    filter_options: FilterOptionsOrSelections,
}

interface SetSearchState {
    (fn: (prevState: SearchState) => SearchState): void
}

// state that will not need an render update
interface InternalSearchState {
    cancel_source: CancelTokenSource,
    strategies: Strategy[],
    next_record_position: string | null,
    is_cancelled: boolean,
}

function errorHandler(setSearchState: SetSearchState) {
    return (error: AxiosError) => {
        if (axios.isCancel(error)) {
            return;
        }
        setSearchState(prevState => ({
            ...prevState,
            is_loading: false,
            error: "Søket feilet. Ta kontakt dersom feilen vedvarer.\n\n" + + error.message,
        }))
        console.error(error);
    }
}

function getAPIFiltersQuery(input: NormalizedInputState): string {
    let query = []

    if (input.fromYear !== null) {
        query.push(`dc.date>=${input.fromYear}`)
    }
    if (input.toYear !== null) {
        query.push(`dc.date<=${input.toYear}`)
    }

    return query.length > 0
        ? " and " + query.join(" and ")
        : "";
}

