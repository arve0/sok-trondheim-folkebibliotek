import { useState } from "react";
import { InputState } from "./input/input-state";
import deepEqual from "fast-deep-equal";
import { set_search_text_in_url_debounced } from "./utils";

export function useNormalizedInputState(input: InputState): [NormalizedInputState, SetNormalizedInputState] {
    let [normalizedInputState, setNormalizedInputState] = useState<NormalizedInputState>(normalize(input));

    function setter(input: InputState) {
        let newState = normalize(input);
        set_search_text_in_url_debounced(newState.searchText);

        // return same state if it has not changed -> avoid re-render
        setNormalizedInputState(prevState =>
            deepEqual(newState, prevState)
                ? prevState
                : newState
        );
    }

    return [normalizedInputState, setter]
}

export interface NormalizedInputState {
    searchText: string,
    fromYear: number | null,
    toYear: number | null,
}

interface SetNormalizedInputState {
    (input: InputState): void
}

function normalize(input: InputState): NormalizedInputState {
    return {
        searchText: normalizeSearchText(input.searchText),
        fromYear: normalizeInteger(input.fromYear),
        toYear: normalizeInteger(input.toYear),
    }
}

/**
 * Removes extra spaces and URL characters (?, &, = and ").
 */
export function normalizeSearchText(text: string): string {
    return text.trim()
        .replace(/ +/g, " ")
        .replace(/[?&="]/g, "")
        .toLowerCase();
}

/**
 * Makes sure the text only contain digits.
 */
export function normalizeInteger(text: string): number | null {
    let digits = text.replace(/[^0-9]/, "");
    if (digits === "") {
        return null;
    }
    return parseInt(digits);
}