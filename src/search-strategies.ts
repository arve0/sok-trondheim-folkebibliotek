import { stem } from "./stemmer";

export interface Strategy {
    (text: string): string | null,
}

const regular_strategies = [
    creator_verbatim,
    creator_includes_all_words,
    title_verbatim,
    title_includes_all_words,
    anywhere_verbatim,
    anywhere_includes_all_words,
]

export function get_strategies() {
    return [
        ...regular_strategies,
        ...all_strategies_with_stemmed_words(),
    ]
}

export function creator_verbatim(text: string): string {
    console.log(`creator_verbatim: ${text}`);
    return `dc.creator="${text}*"`;
}

export function creator_includes_all_words(text: string): string | null {
    console.log(`creator_includes_all_words: ${text}`);
    let query = text
        .split(" ")
        .filter(w => w.length > 2)
        .map(w => `dc.creator=${w}*`)
        .join(" and ");
    if (query === "") {
        return null;
    }
    return query;
}

export function title_verbatim(text: string): string {
    console.log(`title_verbatim: ${text}`);
    return `dc.title="${text}*"`;
}

export function title_includes_all_words(text: string): string | null {
    console.log(`title_includes_all_words: ${text}`);
    let query = text
        .split(" ")
        .filter(w => w.length > 2)
        .map(w => `dc.title=${w}*`)
        .join(" and ");

    if (query === "") {
        return null;
    }

    return query;
}

export function anywhere_verbatim(text: string): string {
    console.log(`anywhere_verbatim: ${text}`);
    return `cql.anywhere="${text}*"`;
}

export function anywhere_includes_all_words(text: string): string | null {
    console.log(`anywhere_includes_all_words: ${text}`);
    let query = text
        .split(" ")
        .filter(w => w.length > 2)
        .map(w => `cql.anywhere=${w}*`)
        .join(" and ");

    if (query === "") {
        return null;
    }

    return query;
}

/**
 * Try all strategies with stemmed words.
 * Will also remove stop words and digits.
 *
 * Examples:
 *
 * "lettleste nynorsk"  -> "lettlest nynorsk"
 * "grunnkurs 2010"     -> "grunnkur"
 * "tatt av kvinnen"    -> "tatt kvinn"
 */
export function all_strategies_with_stemmed_words(): Strategy[] {
    const stemmed_text = (text: string) => text.split(" ").map(stem).join(" ")

    return regular_strategies.map((fn: Strategy) => (text: string) => {
        if (stemmed_text(text) !== text) {
            return fn(stemmed_text(text))
        } else {
            return null
        }
    })
}
