import { SearchResult } from "./sru-request-parsing";
import { Language } from "./Language";

export default function calculate_score(
    record: SearchResult,
    search_text: string,
): number {
    let score = 0;
    score += score_for_field(record.creator, search_text, 1000);

    score += score_for_field(record.title, search_text, 800);

    // new -> higher score
    score += parseInt(record.date) / 1000;

    // language
    // nob > mul > other
    score += record.language === Language.Bokmål ? 50 : 0;
    score += record.language === Language.Nynorsk ? 40 : 0;
    score += record.language === Language.Engelsk ? 30 : 0;

    // by category
    // book -> lyd -> other
    let format = record.format.toLowerCase();
    score += format.includes("bok") && !format.includes("lyd") ? 30 : 0;
    score += format.includes("lyd") ? 20 : 0;
    score += format.includes("blu-ray") ? 15 : 0;
    score += format.includes("dvd") ? 10 : 0;

    return score;
}

function score_for_field(
    field_value: string,
    search_text: string,
    multiplier: number,
): number {
    field_value = field_value.toLowerCase();
    if (!field_value || field_value === "") {
        return 0;
    }
    let score = 0;
    if (field_value === search_text) {
        score += 10;
    } else if (field_value.includes(search_text)) {
        score += 9;
    } else if (all_words_in_text(field_value, search_text)) {
        score += 8;
    } else {
        score += word_count(field_value, search_text);
    }

    return score * multiplier;
}

function all_words_in_text(text: string, words: string): boolean {
    return words.split(" ").every(w => text.includes(w));
}

function word_count(text: string, words: string): number {
    let count = words
        .split(" ")
        .filter(w => w.length > 2)
        .reduce((count, w) => count + (text.indexOf(w) === 0 ? 1 : 0), 0);

    return Math.min(7, count);
}
