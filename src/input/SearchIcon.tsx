import React from "react";

export default function SearchIcon() {
    return (
        <svg
            width={24}
            height={24}
            fill="none"
            stroke="currentColor"
            strokeWidth={1}
            strokeLinecap="round"
            strokeLinejoin="round"
        >
            <circle cx={11} cy={11} r={8} />
            <path d="M23 23l-4.35-4.35" />
        </svg>
    );
}
