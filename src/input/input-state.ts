import { useState } from "react";
import { useNormalizedInputState, NormalizedInputState } from "../normalized-input-state";
import { get_search_text_from_url } from "../utils";

const defaultInputState = {
    searchText: get_search_text_from_url(),
    fromYear: "",
    toYear: "",
}

export function useInputState(): [InputState, NormalizedInputState, SetInputState] {
    let [inputState, setInputState] = useState(defaultInputState);
    let [normalized, setNormalized] = useNormalizedInputState(inputState);

    // sets both input- and normalized input state.
    function setter(map: MapInputState) {
        setInputState(prevState => {
            let newState = map(prevState)
            setNormalized(newState)
            return newState
        })
    }

    return [inputState, normalized, setter];
}

export interface InputState {
    searchText: string,
    fromYear: string,
    toYear: string,
}

export interface SetInputState {
    (fn: MapInputState): void
}

interface MapInputState {
    (prevState: InputState): InputState
}
