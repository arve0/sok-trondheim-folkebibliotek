import React from "react";
import { InputState, SetInputState } from "./input-state";
import SearchIcon from "./SearchIcon";
import { updateValue } from "../utils";
import "./SearchInput.css";

export const SearchInput: React.FC<SearchInputProps> = ({
           inputState,
           setInputState,
           searchEnabled,
           setSearchEnabled,
       }) => (
           <div id="tfbs-search">
               <input
                   id="tfbs-search-input"
                   aria-label="Søk etter bok, film, musikk eller annet"
                   placeholder="Søk etter bok, film, musikk eller annet"
                   type="text"
                   value={inputState.searchText}
                   onChange={e => updateValue(e, "searchText", setInputState)}
                   onFocus={() => setSearchEnabled(true)}
               />
               <span id="tfbs-search-icon">
                   <SearchIcon />
               </span>
               {searchEnabled && (
                   <button
                       id="tfbs-lukk"
                       onClick={() => setSearchEnabled(false)}
                   >
                       ╳ Lukk
                   </button>
               )}
               <a href="https://www.tfb.no/cgi-bin/m2" id="tfbs-avansert-sok">
                   Avansert søk
               </a>
           </div>
       );

interface SearchInputProps {
    inputState: InputState,
    setInputState: SetInputState,
    searchEnabled: boolean,
    setSearchEnabled: (e: boolean) => void,
}