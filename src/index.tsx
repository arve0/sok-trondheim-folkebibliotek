import React from "react";
import ReactDOM from "react-dom";
import App from "./App";

var activateInterval = setInterval(activateTFBS, 200);

function activateTFBS() {
    // remove backdrop whiteout if present
    let whiteout = document.querySelector(".whiteout");
    if (whiteout !== null) {
        whiteout.remove();
    }

    let oldSearch = document.querySelector(".searchLib");
    if (oldSearch === null) {
        console.error("Fant ikke gammelt søk '.searchLib'");
        return;
    }

    // find first row in parent of .searchLib
    let row = oldSearch.parentElement;
    while (row !== null && !row!.classList.contains("row")) {
        row = row.parentElement;
    }
    if (row === null) {
        return;
    }

    // consider search activated
    clearInterval(activateInterval);

    Array.from(row.children).forEach(child => {
        row!.removeChild(child)
    });

    let placeholder = document.createElement("div");
    placeholder.id = "tfbs-root";
    row.appendChild(placeholder);

    ReactDOM.render(<App />, placeholder);

    hijackSearchButton();
    hijackFooterSearch();
}

/**
 * Changes event listener for menu search button,
 * such that is works with both old and new search.
 */
function hijackSearchButton() {
    if (typeof $ !== "function") {
        throw new Error("Tried disabling event listeners registered with jQuery, but jQuery not found")
    }
    $(".top-menu-search a").off();
    $(".top-menu-search a").click(function (event) {
        event.preventDefault();
        showSearch();
        $("#tfbs-search-input").focus();
        $("#tfbs-search-input").on("blur", disableSearch);
    });
}

function showSearch() {
    $(".top.noteditorial").show();
    $(".top.noteditorial").css("position", "absolute");
    $(".top.noteditorial").css("left", 0);
    $(".top.noteditorial").css("right", 0);
}

// hack: hide search when input disabled (lost focus + no search text)
function disableSearch() {
    // wait for react to update input value state
    setTimeout(function () {
        var value = $("#tfbs-search-input").val();
        if (value === "") {
            $(".top.noteditorial").hide();
        }
    }, 50)
}

function hijackFooterSearch() {
    if (typeof $ !== "function") {
        throw new Error("Tried disabling event listeners registered with jQuery, but jQuery not found")
    }
    $(".search input").off();
    $(".search input").keyup(function (event) {
        event.preventDefault();
        if (event.key !== "Enter") {
            return;
        }
        var searchText = $(".search input").val();
        if (searchText !== "") {
            window.location.assign("/#tfbs:" + searchText);
        }
    });
}
