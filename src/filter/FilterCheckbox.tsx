import React from "react";
import { SetFilterState, FilterState } from "./filter-state";
import "./FilterCheckbox.css";
import { FilterOptionsOrSelections } from "./filter-options";

export function FilterCheckbox({
    value,
    key,
    filterState,
    setFilterState,
}: FilterCheckboxProps) {
    let isChecked = filterState.selected[key].includes(value);
    let className = isChecked ? "checked" : "";

    return (
        <label
            className={"tfbs-label-inline " + className}
            key={value}
            onClick={() =>
                setFilterState(prevState => ({
                    ...prevState,
                    selected: {
                        ...prevState.selected,
                        [key]: toggleSelected(value, filterState.selected[key]),
                    },
                }))
            }
        >
            <span />
            {value}
        </label>
    );
}

interface FilterCheckboxProps {
    value: string;
    key: keyof FilterOptionsOrSelections;
    filterState: FilterState;
    setFilterState: SetFilterState;
}

export function toggleSelected<T>(value: T, values: T[]): T[] {
    let position = values.indexOf(value);

    if (position === -1) {
        values.push(value);
    } else {
        values.splice(position, 1);
    }

    return values;
}

export const FilterCheckboxSingle: React.FC<FilterCheckboxSingleProps> = ({
    label,
    checked,
    onToggle,
}) => {
    let className = checked ? "checked" : "";

    return (
        <label className={"tfbs-filter-checkbox-single " + className} onClick={() => onToggle()}>
            {label}
            <span />
        </label>
    );
};

interface FilterCheckboxSingleProps {
    label: string;
    checked: boolean;
    onToggle: () => void;
}
