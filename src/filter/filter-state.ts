import { useEffect, useState } from "react";
import { SearchState } from "../search-state";
import { defaultFilterOptions, FilterOptionsOrSelections } from "./filter-options";

const defaultFilterState: FilterState = {
    includeTranslations: false,
    // options available in search results
    selected: defaultFilterOptions,
}

// state of all filters
export function useFilterState(searchState: SearchState): [FilterState, SetFilterState] {
    const [filterState, setFilterState] = useState(defaultFilterState)

    // includeTranslations depending on search results
    // lot of translations => show translations
    // few translations => hide translations
    useEffect(() => {
        let results = searchState.search_results;
        if (results.length === 0) {
            // reset to not include translations upon new search
            setFilterState(prevState => ({
                ...prevState,
                includeTranslations: false
            }));
        } else if (results.length > 0) {
            // if more then 60% of results are translated -> show them
            // high percentage suggests that origin language is not norwegian
            let number_translated = results.filter(r => r.original_title !== undefined).length;
            let percent_translated = number_translated / results.length;

            if (percent_translated > 0.6 && !filterState.includeTranslations) {
                setFilterState(prevState => ({
                    ...prevState,
                    includeTranslations: true
                }));
            }
        }
    }, [searchState])

    return [filterState, setFilterState];
}

export interface FilterState {
    includeTranslations: boolean,
    selected: FilterOptionsOrSelections,
}

export interface SetFilterState {
    (fn: (filters: FilterState) => FilterState): void
}
