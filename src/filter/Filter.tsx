import React, { useState } from "react";
import { FilterOptionsOrSelections } from "./filter-options";
import { FilterState, SetFilterState } from "./filter-state";
import { FilterCheckbox } from "./FilterCheckbox";
import { SearchState } from "../search-state";
import "./Filter.css";

export const Filter: React.FC<FilterProps> = ({
    name,
    keyName,
    searchState,
    filterState,
    setFilterState,
}) => {
    let hasMoreThenFour = searchState.filter_options[keyName].length > 4;
    let [shouldLimit, setShouldLimit] = useState(true);
    let options = searchState.filter_options[keyName].slice(0, shouldLimit ? 4 : -1);

    return (
        <>
            <label>{name}</label>
            {hasMoreThenFour && (
                <span className="tfbs-show-more" onClick={() => setShouldLimit(!shouldLimit)}>
                    {(shouldLimit && <span>Vis alle</span>) ||
                        (!shouldLimit && <span>Vis færre</span>)}
                </span>
            )}
            {options.map(value =>
                FilterCheckbox({
                    value: value,
                    key: keyName,
                    filterState,
                    setFilterState,
                }),
            )}
        </>
    );
};

interface FilterProps {
    name: string;
    keyName: keyof FilterOptionsOrSelections;
    searchState: SearchState;
    filterState: FilterState;
    setFilterState: SetFilterState;
}
