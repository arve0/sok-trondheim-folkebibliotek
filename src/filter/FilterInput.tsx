import React from "react";
import { updateValue } from "../utils";
import { FilterState, SetFilterState } from "./filter-state";
import { SetInputState, InputState } from "../input/input-state";
import "./FilterInput.css";
import { SearchState } from "../search-state";
import { FilterCheckboxSingle } from "./FilterCheckbox";
import { Filter } from "./Filter";

export const FilterInput: React.FC<FilterInputProps> = ({
           inputState,
           setInputState,
           filterState,
           setFilterState,
           searchState,
       }) => (
           <div id="tfbs-filter">
               <h2>Filter</h2>
               <label htmlFor="tfbs-year-from">Årstall</label>
               <br />
               <input
                   id="tfbs-year-from"
                   type="text"
                   placeholder="Fra"
                   value={inputState.fromYear}
                   onChange={e => updateValue(e, "fromYear", setInputState)}
               />
               <br />
               <input
                   type="text"
                   placeholder="Til"
                   value={inputState.toYear}
                   onChange={e => updateValue(e, "toYear", setInputState)}
               />
               <br />
               {searchState.search_results.length > 0 && (
                    <>
                        <Filter
                            name="Språk"
                            keyName="languages"
                            searchState={searchState}
                            filterState={filterState}
                            setFilterState={setFilterState}
                        />
                        <FilterCheckboxSingle
                            label="Inkluder oversettelser"
                            checked={filterState.includeTranslations}
                            onToggle={() => setFilterState(prevState => ({
                                ...prevState,
                                includeTranslations: !prevState.includeTranslations,
                            }))}
                       />
                        <Filter
                            name="Forfatter"
                            keyName="creators"
                            searchState={searchState}
                            filterState={filterState}
                            setFilterState={setFilterState}
                        />
                        <Filter
                            name="Medie"
                            keyName="formats"
                            searchState={searchState}
                            filterState={filterState}
                            setFilterState={setFilterState}
                        />
                   </>
               )}
           </div>
       );

interface FilterInputProps {
    inputState: InputState;
    setInputState: SetInputState;
    filterState: FilterState;
    setFilterState: SetFilterState;
    searchState: SearchState;
}