import { language_to_string } from "../Language";
import { unique } from "../utils";
import { SearchState } from "../search-state";

export const defaultFilterOptions: FilterOptionsOrSelections = {
    languages: [],
    creators: [],
    formats: [],
};

export function createFilterOptions(searchState: SearchState): FilterOptionsOrSelections {
    if (searchState.search_results.length > 0) {
        let languages = unique(searchState.search_results.map(r => language_to_string(r.language)));
        let creators = unique(searchState.search_results.map(r => r.creator)).filter(c => c !== "");
        let format = unique(searchState.search_results.map(r => r.format)).filter(c => c !== "");

        return { languages, creators, formats: format };
    }
    return defaultFilterOptions;
}

export interface FilterOptionsOrSelections {
    languages: string[];
    creators: string[];
    formats: string[];
}
