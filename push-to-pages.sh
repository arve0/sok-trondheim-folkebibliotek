#!/bin/sh

PUBLIC_FOLDER=sok-trondheim-folkebibliotek
COMMIT=$(git rev-parse HEAD)

git clone git@bitbucket.org:arve0/arve0.bitbucket.io.git pages

rm -fr pages/$PUBLIC_FOLDER
mv build pages/$PUBLIC_FOLDER

git config --global user.email "pipeline@bitbucket.org"
git config --global user.name "Bitbucket Pipeline"

git -C pages add --all
git -C pages commit -m "https://bitbucket.org/arve0/sok-trondheim-folkebibliotek/commits/$COMMIT"

git -C pages push origin master