const manifest = require("./build/asset-manifest.json")
const fs = require("fs")
const rmrf = require("rimraf")
const mkdirp = require("mkdirp")
const path = require("path")
const process = require("child_process")
const package = require("./package.json")

let OUTPUT_DIR = package.name;
let OUTPUT_FILE = `${OUTPUT_DIR}-${package.version}.zip`

let files = manifest.entrypoints.map(f => "build/" + f)
files.push("build/asset-manifest.json")
files.push("build/internal-bootstrap.js")

fs.readdirSync("build/static/media")
    .forEach(f => files.push("build/static/media/" + f))

rmrf.sync(OUTPUT_DIR)
fs.mkdirSync(OUTPUT_DIR)
try {
    fs.unlinkSync(OUTPUT_FILE)
} catch (e) { /* no-op */ }

files.forEach(filename => {
    let destination = filename.replace("build/", OUTPUT_DIR + "/")
    let directory = path.dirname(destination)
    mkdirp.sync(directory)

    fs.copyFileSync(filename, destination)
})


process.execSync(`zip -r ${OUTPUT_FILE} ${OUTPUT_DIR}`)
rmrf.sync(OUTPUT_DIR)
