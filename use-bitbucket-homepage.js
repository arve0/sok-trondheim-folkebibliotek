const package = require("./package.json")
const fs = require("fs")

package.homepage = "https://arve0.bitbucket.io/sok-trondheim-folkebibliotek/"

fs.writeFileSync("package.json", JSON.stringify(package, null, 2))