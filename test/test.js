const assert = require("assert")
const configuration = require("../package.json")
const puppeteer = require("puppeteer");

// configuration
const mainPage = `http://localhost:3000/#tfbs`
const headless = false  // false: show browser, true: hide browser
const slowMo = false  // true: each browser action will take 100 milliseconds
    ? 100
    : 0
const SEARCH_INPUT = "#tfbs-search-input";

// globals
let browser = null
let page = null

before(async function () {
    this.timeout(10 * 1000) // starting browser may take more than 2 seconds

    browser = await puppeteer.launch({ headless, slowMo })
    page = (await browser.pages())[0]
    await page.goto(mainPage)

    page.on("console", async function (msg) {
        if (msg.type() === "error" && msg.args().length) {
            let args = await Promise.all(msg.args().map(arg => arg.jsonValue()))
            console.error("Browser console.error:", ...args)
        } else {
            if (msg._text !== undefined) {
                console.log(msg._text)
            }
        }
    })
})

after(function () {
    if (slowMo !== 0) {
        setTimeout(() => browser.close(), 2000);
    } else {
        browser.close();
    }
})

beforeEach(async function () {
    await reset_and_activate_input()
})

async function reset_and_activate_input() {
    await waitFor({ selector: SEARCH_INPUT })
    // page ready
    await page.$eval(SEARCH_INPUT, input => {
        // to update react state, input must have focus before setting value
        input.blur();
        input.focus();
    })
    await page.$eval(SEARCH_INPUT, input => input.value = "")
}

describe("tests", function () {
    this.timeout(slowMo === 0 ? 3000 : 0)

    it("har søkefelt med label", async function () {
        await page.keyboard.type("as")
        let input_value = await page.$eval(SEARCH_INPUT, input => input.value)
        assert.equal(input_value, "as")
    })

    it("har en lukk-knapp", async function () {
        await page.keyboard.type("ragde")
        await waitFor({ selector: "#tfbs-results" })

        await waitFor({ selector: "button", text: "Lukk", click: true });

        try {
            await waitFor({ selector: "#tfbs-results", timeout: 100 })
        } catch (error) {
            return;
        }
        assert.fail("found results on page")
    })

    it("søk på forfatter ordrett skal gi treff", async function () {
        await page.keyboard.type("anne birkefeldt ragde")
        await waitFor({ text: "Anne Birkefeldt Ragde" })
    })

    it("søk på delvis forfatter skal gi treff", async function () {
        await page.keyboard.type("anne b ragde")
        await waitFor({ text: "Anne Birkefeldt Ragde" })
    })

    it("søk på tittel ordrett skal gi treff", async function () {
        await page.keyboard.type("tatt av kvinnen")
        await waitFor({ text: "Tatt av kvinnen" })
    })

    it("søk på delvis tittel skal gi treff", async function () {
        await page.keyboard.type("Farmor julemat tradisjoner")
        await waitFor({ text: "Farmor si oppskrift : julemat og tradisjoner" })
    })

    it("søk skal ikke gi duplikater", async function () {
        await page.keyboard.type("tatt av kvinnen")
        await waitFor({ text: "Tatt av kvinnen (DVD, 2007)" })
        let titler = await alle_titler_søketreff()
        let dvd_2007 = titler.filter(t => t === "Tatt av kvinnen (DVD, 2007)")
        assert.equal(dvd_2007.length, 1)
    })

    it("søk med mange treff", async function () {
        await page.keyboard.type("ragde")
        await vent_på_søketreff()
        let første_treff = await alle_titler_søketreff()

        await waitFor({ text: "Hent flere", click: true })
        let alle_treff = await alle_titler_søketreff()

        while (første_treff.length === alle_treff.length) {
            alle_treff = await alle_titler_søketreff()
        }
    })

    it("søk skal sortere etter dato", async function () {
        await page.keyboard.type("tatt av kvinnen")
        await vent_på_søketreff()
        let results = await alle_titler_søketreff()

        let position_2001 = results.findIndex(title => title.includes("2001)"))
        let position_2000 = results.findIndex(title => title.includes("2000)"))

        assert(position_2001 < position_2000)
    })

    it("søk skal sortere bøker foran lydbøker foran dvd", async function () {
        await page.keyboard.type("tatt av kvinnen")
        await vent_på_søketreff()
        let results = await alle_titler_søketreff()

        let bok = results.findIndex(title => title.includes("Bok"))
        let lydbok = results.findIndex(title => title.includes("Lydbok"))
        let dvd = results.findIndex(title => title.includes("DVD"))

        assert(bok < lydbok)
        assert(lydbok < dvd)
    })

    it("søk skal sortere blu-ray foran dvd", async function () {
        await page.keyboard.type("blade runner 2049")
        await vent_på_søketreff()
        let results = await alle_titler_søketreff()

        let blueray = results.findIndex(title => title.includes("Blu-ray"))
        let dvd = results.findIndex(title => title.includes("DVD"))

        assert(blueray < dvd)
    })

    it("søk på beskrivelse ordrett", async function () {
        this.timeout(10 * 1000)
        await page.keyboard.type("Psykologisk spenningsroman med kriminaletterforsker Jonfinn Valmann")
        await waitFor({ text: "Stammeren" })
    })

    it("søk på delvis beskrivelse", async function () {
        this.timeout(10 * 1000)
        await page.keyboard.type("Psykologisk kriminaletterforsker Valmann")
        await waitFor({ text: "Stammeren" })
    })

    it("søk skal nullstille seg", async function () {
        this.timeout(10 * 1000)
        await page.keyboard.type("blade runner 2049")
        await vent_på_søketreff()

        await reset_and_activate_input()
        await page.keyboard.type("tatt av kvinnen")
        await waitFor({ text: "Tatt av kvinnen" })

        let blade_runner = (await alle_titler_søketreff())
            .filter(title => title.includes("Blade runner 2049"))

        assert.equal(blade_runner.length, 0)
    })

    it("søketreff skal ha 'vis mer'-knapp", async function() {
        this.timeout(10 * 1000);
        await page.keyboard.type("anne birkefeldt ragde");
        await waitFor({ text: "Vis mer", click: true });
        await waitFor({ text: "Her finner du den" });
        await page.goto(mainPage);
    });

    it("skal fjerne klassesett", async function() {
        await page.keyboard.type("perks of being");
        await vent_på_søketreff();
        let titler = await alle_titler_søketreff();
        // 2012-utgaven er for øyeblikket kun i klassesett
        let bok_2012 = titler.filter(t => t.includes("Bok, 2012"));
        assert.equal(bok_2012.length, 0);
    });

    it("lim-inn skal fjerne forrige søkeresultat", async function() {
        await page.keyboard.type("anne ragde");
        await vent_på_søketreff();

        // simuler lim-inn ved å sette verdi og deretter trykke en tast
        await page.$eval(SEARCH_INPUT, input => input.value = "jo nesb");
        await page.keyboard.type("ø");

        await vent_på_søketreff();
        let titler = await alle_titler_søketreff();
        let bok_av_anne_ragde = titler.filter(t => t.includes("Datteren"));
        assert.equal(bok_av_anne_ragde.length, 0);
    });

    it("skal hente søketekst fra URL", async function() {
        await page.goto(mainPage + ":anne ragde");
        await page.reload();
        await vent_på_søketreff();

        let titler = await alle_titler_søketreff();
        assert(titler.length > 5);
    });

    it("skal sette søketekst til URL", async function() {
        await page.keyboard.type("anne");
        await page.waitFor(500);

        let hash = await page.evaluate(() => window.location.hash);
        assert.equal(hash, "#tfbs:anne");
    });

    it("skal unngå race-conditions", async function() {
        await page.keyboard.type("jo nesbø");
        await vent_på_søketreff();
        let titler1 = await alle_titler_søketreff();

        await page.keyboard.press("Backspace");
        await page.waitFor(200);
        await page.keyboard.type("ø");
        await vent_på_søketreff();
        let titler2 = await alle_titler_søketreff();

        assert.deepEqual(titler1, titler2);
    });

    it("skal vise oversettelser når det er flertall av de i søkeresultatet", async function() {
        await page.keyboard.type("gutta i trehuset");
        await vent_på_søketreff();
        let titler = await alle_titler_søketreff();
        assert(titler.length > 5, "skal være flere enn fem treff");
    });

    it("skal ikke vise oversettelser som standard", async function() {
        await page.keyboard.type("Liebhaberne");
        await vent_på_søketreff();
        let titler = await alle_titler_søketreff();
        let polske_oversettelserr = titler.filter(t => t.includes("Kochankowie"))
        assert.equal(polske_oversettelserr.length, 0);
    });

    it("skal ha mulighet til å vise oversettelser", async function() {
        await page.keyboard.type("Liebhaberne");
        await waitFor({ text: "Inkluder oversettelser", click: true })
        await vent_på_søketreff();
        let titler = await alle_titler_søketreff();
        let polske_oversettelserr = titler.filter(t => t.includes("Kochankowie"))
        assert.equal(polske_oversettelserr.length, 1);
    });

    it("skal gi beskjed når det ikke er treff", async function() {
        this.timeout(5 * 1000);
        await page.keyboard.type("aasdfasdfasdf");
        await waitFor({ text: "ingen treff" });
    });

    it("skal fjerne årstall fra søk og filtrere lokalt", async function() {
        this.timeout(5 * 1000);
        await page.keyboard.type("grunnkurs 2010");
        await vent_på_søketreff();
        let titler = await alle_titler_søketreff();
        let titler_2010 = titler.filter(t => t.includes("2010"));

        assert.deepEqual(titler, titler_2010);
    });

    it("skal filtrere på årstall", async function() {
        await page.keyboard.type("ragde");
        await waitFor({ text: "Årstall", click: true });
        await page.keyboard.press("Tab");
        await page.keyboard.type("2017");

        await vent_på_søketreff();
        let titler = await alle_titler_søketreff();

        assert(titler[0].includes("2017"));

        // clean up
        await waitFor({ text: "Årstall", click: true });
        await page.keyboard.press("Tab");
        await page.keyboard.press("Delete");
    });

    it("skal filtrere på språk", async function() {
        await page.keyboard.type("ragde");
        await waitFor({ text: "Polsk", click: true });

        await page.waitFor(100);
        await vent_på_søketreff();
        let titler = await alle_titler_søketreff();

        assert(titler[0].includes("Kochankowie"));

        // clean up
        await waitFor({ selector: "label", text: "Polsk", click: true });
    });

    it("skal filtrere på forfatter", async function() {
        await page.keyboard.type("olsen");
        await waitFor({ selector: "label", text: "Sigrid Olsen", click: true });

        await page.waitFor(100);
        await vent_på_søketreff();
        let forfattere = unique(await alle_forfattere_søketreff());

        assert.equal(forfattere.length, 1);
        assert.equal(forfattere[0], "Sigrid Olsen");

        // clean up
        await waitFor({ selector: "label", text: "Sigrid Olsen", click: true });
    });

    it("skal filtrere på medie", async function() {
        await page.keyboard.type("ragde");
        await waitFor({ selector: "label", text: "Lydbok", click: true });

        await page.waitFor(100);
        await vent_på_søketreff();
        let titler = await alle_titler_søketreff();

        assert(titler[0].includes("Lydbok"));

        // clean up
        await waitFor({ selector: "label", text: "Lydbok", click: true });
    });

    it("skal ha 'vis alle' på filtre", async function() {
        await page.keyboard.type("olsen");
        await vent_på_søketreff()
        let options_before = (await page.$$(".tfbs-label-inline")).length;

        await waitFor({ text: "Vis alle", click: true });
        await waitFor({ text: "Vis færre" });
        let options_after = (await page.$$(".tfbs-label-inline")).length;
        assert(options_after > options_before);

        await waitFor({ text: "Vis færre", click: true });
        await waitFor({ text: "Vis alle" });
        let options_less = (await page.$$(".tfbs-label-inline")).length;
        assert(options_less === options_before);
    });

    it('skal trimme ord til stammen', async function() {
        this.timeout(10 * 1000);
        await page.keyboard.type("lettleste nynorsk"); // -> lettlest, which is a category
        await vent_på_søketreff();
        let titler = await alle_titler_søketreff();

        assert(titler.length > 5);
    });

})

async function vent_på_søketreff() {
    await waitFor({ selector: ".tfbs-entry-title" })
}

function alle_titler_søketreff() {
    return page.$$eval(".tfbs-entry-title", titles => titles.map(title => title.innerText))
}

function alle_forfattere_søketreff() {
    return page.$$eval(".tfbs-creator", creators => creators.map(creator => creator.innerText.replace("Forfatter: ", "")))
}

/**
 * Waits for a visible element containing given text, possibly clicks it.
 *
 * @param {object} params - What to wait for, in which selector, if it should be clicked and how long to wait.
 * @param {string} params.text - What text to wait for.
 * @param {string} params.selector - What selector to use, defaults to any element: `*`.
 * @param {bool} params.click - Wheter to click when found.
 * @param {number} params.timeout - How long to wait in milliseconds.
 */
async function waitFor({ text = "", selector = "*", click = false, focus = false, timeout = 5000 }) {
    const start = Date.now()

    while ((Date.now() - start) < timeout) {
        let frames = await page.frames()
        let scopes = [page, ...frames]
        for (let scope of scopes) {
            let result
            try {
                result = await scope.evaluate(pageFunction, text, selector, click)
            } catch (err) {
                // probably lost execution context, break and get new scopes
                break
            }
            if (result) {
                return true
            }
        }
    }

    throw new Error(`"${text}" not found on page in selector "${selector}", waited ${timeout} milliseconds.`)

    function pageFunction (text, selector, click) {
        let match = findElement(text, selector)

        if (match) {
            if (click) {
                match.click()
            }
            if (focus) {
                match.focus()
            }
            return true
        }
        return false

        function findElement(text, selector) {
            let matchingElements = Array.from(document.querySelectorAll(selector))
                .filter(element => element.textContent.includes(text))
                .sort(shortestTextVisibleIfSameLength) // shortest text first, e.g. "best" search result

            if (matchingElements.length > 0 && isVisible(matchingElements[0])) {
                return matchingElements[0]
            }

            let shadowedElements = Array.from(document.querySelectorAll(selector))
                .filter(element => element.shadowRoot)
                .flatMap(element => Array.from(element.shadowRoot.querySelectorAll(selector)))
                .filter(element => element.textContent.includes(text))
                .sort(shortestTextVisibleIfSameLength)

            if (shadowedElements.length > 0 && isVisible(shadowedElements[0])) {
                return shadowedElements[0]
            }

            return null
        }

        function shortestTextVisibleIfSameLength(a, b) {
            let difference = a.textContent.length - b.textContent.length;
            if (difference === 0) {
                let aVisible = isVisible(a)
                let bVisible = isVisible(b)

                if (aVisible && !bVisible) {
                    return -1
                } else if (!aVisible && bVisible) {
                    return 1
                } else {
                    return 0
                }
            }
            return difference
        }

        function isVisible(element) {
            return element.offsetParent !== null
        }
    }
}

function unique(values) {
    return Array.from((new Set(values)).values())
}
