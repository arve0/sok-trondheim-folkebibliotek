# Tester
Dette er et testoppsett for ende-til-ende-testing med firefox/chromium som bruker

- [puppeteer](https://www.npmjs.com/package/puppeteer/) og
- [mocha](https://www.npmjs.com/package/mocha).

Testoppsettet vil

1. Starte firefox eller chromium gjennom puppeteer
2. Kjøre testene med mocha

## Hvordan kjøre tester?
Forsikre at dev-server kjører:
```sh
npm start
```

Kjør testene i en annen terminal:
```sh
$ npm test

> sok-trondheim-folkebibliotek@0.1.0 test /Users/arve/KNOWIT-git/sok-trondheim-folkebibliotek
> mocha test/test.js



  tests
    ✓ har søkefelt med label (43ms)
    ✓ søk på forfatter ordrett skal gi treff (770ms)
    ✓ søk på delvis forfatter skal gi treff (1658ms)
    ✓ søk på tittel ordrett skal gi treff (1029ms)
    ✓ søk på delvis tittel skal gi treff (1353ms)
    ✓ søk skal ikke gi duplikater (882ms)
    ✓ søk med mange treff (786ms)
    ✓ søk skal sortere etter dato (976ms)
    ✓ søk skal sortere bøker foran lydbøker foran dvd (970ms)
    ✓ søk skal sortere blu-ray foran dvd (885ms)
    ✓ søk på beskrivelse ordrett (4559ms)
    ✓ søk på delvis beskrivelse (1848ms)
    ✓ søk skal nullstille seg (1733ms)
    ✓ søketreff skal ha "vis mer"-knapp (2716ms)


  14 passing (23s)
```

## Hvordan kjøre en spesifikk test?
```sh
$ npm test -- -f duplikater

> sok-trondheim-folkebibliotek@0.1.0 test /Users/arve/KNOWIT-git/sok-trondheim-folkebibliotek
> mocha test/test.js "-f" "duplikater"



  tests
    ✓ søk skal ikke gi duplikater (963ms)


  1 passing (3s)
```

## Hvordan ser testene ut?
```js
it('har søkefelt med label', async function () {
    await page.keyboard.type('as')
    let input_value = await page.$eval("#tfbs-input", input => input.value)
    assert.equal(input_value, "as")
})
```

## Konfigurasjon
Les toppen av [test.js](test.js):

```js
// configuration
const mainPage = `http://localhost:3000/`
const headless = false  // false: show browser, true: hide browser
const slowMo = true  // true: each browser action will take 100 milliseconds
    ? 100
    : 0
```
