/**
 * Bruk:
 *
 * <script src="https://arve0.bitbucket.io/sok-trondheim-folkebibliotek/external-bootstrap.js"></script>
 */
var TFBS_CORS = "https://cors.seljebu.no/"
var TFBS_BASE_URL = TFBS_CORS + "https://arve0.bitbucket.io/sok-trondheim-folkebibliotek/";
var TFBS_MANIFEST = TFBS_BASE_URL + "asset-manifest.json";

window.addEventListener('DOMContentLoaded', bootstrapTFBS);

function bootstrapTFBS() {
    fetch(TFBS_MANIFEST)
        .then(r => {
            if (r.status !== 200) {
                throw new Error("Unable to fetch asset-manifest for TFBS");
            }
            return r.json();
        })
        .then(addFilesFromManifestToDocument)
}

function addFilesFromManifestToDocument(manifest) {
    return manifest.entrypoints
        .map(filename => TFBS_BASE_URL + filename)
        .forEach(entryPointToElements)
}

function entryPointToElements(entrypoint) {
    if (entrypoint.match(/\.css$/) !== null) {
        appendStyleElement(entrypoint);
    } else if (entrypoint.match(/\.js$/) !== null) {
        appendScriptElement(entrypoint);
    } else {
        console.error("Unknown asset type:", entrypoint);
    }
}

function appendStyleElement(href) {
    var style = document.createElement("link");
    style.rel = "stylesheet";
    style.type = "text/css";
    style.href = href;
    document.head.appendChild(style);
}

function appendScriptElement(src) {
    var script = document.createElement("script");
    script.type = "text/javascript";
    script.src = src;
    document.body.appendChild(script);
}
